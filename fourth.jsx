/*
    W poniższym komponencie `console.log('render')` pokazuje się około 100 razy po każdym kliknięciu guzika do usunięcia elementu listy.

    Zachowując podział na komponenty App i ListElement, oraz wygląd i działanie kodu po stronie użytkownika, co można zrobić aby:
    1. Wywołwać jak najmniej przerenderowań? (Czyli ograniczyć liczbę logów 'render' na konsolę, bez usuwania console.log 😄)
    2. Przyśpieszyć działanie tego kodu?

    Czy coś jeszcze można tu poprawić?
*/

import React, { PureComponent } from 'react'

const ListElement = memo(({ number, content }) => {
    console.log('render')
    return (
        <li>{number}. {content}</li>
    )
})


class App extends PureComponent {

    state = {
        elements: (new Array(100))
            .fill()
            .map(() => `I'm a list element! ${Math.floor(Math.random()*1000)}`)
            .map((value, index) => ({
                id: shortid.generate(),
                index,
                content: value
            }))
    }

    handleClick = () => {
        this.setState({
            elements: this.state.elements.slice(1)
        })
    }

    render() {
        console.log('render')
        return (
            <div>
                <button onClick={this.handleClick}>
                    Remove first list element
                </button>
                <ul style={{ listStyle: 'none' }}>
                    {this.state.elements.map(value => (
                        <ListElement 
                            key={value.index} 
                            content={value.content} 
                            number={value.index+1} />
                        )
                    )}
                </ul>
            </div>
        )
    }

}

// With 🥰 to functional programing
// Functional style λ
import { useState, memo } from 'react'
import * as shortid from 'shortid'

const transformData = elements =>
    elements.map((value, index) => ({
        id: shortid.generate(),
        index,
        content: value
    }))

const ListElement = memo(({ number, content }) => {
    console.log('render')
    return (
        <li>{number}. {content}</li>
    )
})

const App = () => {

    const defaultElements = (new Array(100))
        .fill()
        .map(() => `I'm a list element! ${Math.floor(Math.random() * 1000)}`)

    const [ elements, setElements ] = useState(
        transformData(defaultElements)
    )

    const handleClick = () => {
        setElements(elements.slice(1))
    }

    console.log('render')
    return (
        <div>
            <button onClick={handleClick}>
                Remove first list element
            </button>
            <ul style={{ listStyle: 'none' }}>
                {elements.map(value => (
                    <ListElement 
                        key={value.id} 
                        content={value.content} 
                        number={value.index + 1} 
                    />
                ))}
            </ul>
        </div>
    )
} 
