/*
    Ten komponent nie działa 😱 Jak trzeba go naprawić? Dlaczego nie działał?
    Co w tym kompnencie można zmienić, żeby był ładniej/sensowniej napisany?
*/

import React, { Component } from 'react'

class App extends Component {

    static defaultProps = {
        name: 'Anonymous'
    }
    
    constructor(props) {
        super(props)
        this.state = {
            name: props.name
        }
    }

    handleChange = e => {
        this.setState({
            name: e.target.value || this.props.name
        })
    }

    render() {
        return (
            <div>
                <input type="text" onChange={this.handleChange} />
                <p>Hello {this.state.name}</p>
            </div>
        )
    }

}

// With 🥰 to functional programing
// Functional style λ
import { useState } from 'react'

const App = ({
    name = 'Anonymous'
}) => {

    const [ inputName, setInputName ] = useState(name)

    return (
        <div>
            <input 
                type="text"
                onChange={({ target }) => setInputName(target.value || name)}
            />
            <p>Hello {inputName}</p>
        </div>
    )

}