/*
    W reducerze createApp przy obsłudze każdej z akcji w kodzie jest jakiś problem.
    Jak można poprawić każdy z nich? Dlaczego z ich kodem jest coś nie tak?
*/

const initialState = {
    visibilityFilter: 'none',
    products: [
        {
            id: 305,
            price: 100,
            lastSold: '2020-02-12T13:19:15.784Z'
        },
        {
            id: 308,
            price: 500,
            lastSold: '2020-02-14T11:12:18.113Z'
        },
        {
            id: 309,
            price: 299,
            lastSold: '2020-02-14T14:32:06.361Z'
        },
    ]
}

const updateProduct = (id, newProduct) => 
    product => {
        if (product.id === id)
            return {
                ...product,
                ...newProduct
            }
        return product
    }

function createApp(state = initialState, { type, data }) {

    switch(type) {

        case SET_VISIBILITY_FILTER:
            return { 
                ...state, 
                visibilityFilter: data.filter 
            }

        case CHANGE_PRODUCT_PRICE:
            return { 
                ...state, 
                products: state.products
                    .map(updateProduct(data.id, { 
                        price: data.newPrice 
                    }))
            }

        case UPDATE_LAST_SOLD:
            return { 
                ...state, 
                products: state.products
                    .map(updateProduct(data.id, { 
                        lastSold: new Date().toISOString() 
                    }))
            } 

        default:
            return state

    }
}