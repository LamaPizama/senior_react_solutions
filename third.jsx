/*
    Poniższy komponent wygląda, jakby miał działać, ale... nie działa.
    Jak można go naprawić? Dlaczego nie działa?
*/


import React, { Component } from 'react'

class App extends Component {

    state = { 
        search: '' 
    }

    handleChange = event => {
        event.persist()
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => {
            this.setState({
                search: event.target.value
            })

        }, 250)
    }

    render() {
        return (
            <div>
                <input type="text" onChange={this.handleChange} />
                {this.state.search && <p>Search for: {this.state.search}</p>}
            </div>
        )
    }

}

// With 🥰 to functional programing
// Functional style λ
import { useState } from 'react'

const App = () => {

    const [ search, setSearch ] = useState()
    let timeout = null

    const handleChange = event => {
        event.persist()
        clearTimeout(timeout)
        timeout = setTimeout(() => {
            setSearch(event.target.value)
        }, 250)
    }

    return (
        <div>
            <input type="text" onChange={handleChange} />
            {search && <p>Search for: {search}</p>}
        </div>
    )
}