/*
    Poniższy kod się nie uruchamia i ma w sobie kilka błędów. Jak go naprawić? Co można zmienić w tym kodzie?
*/

import React, { Component } from 'react'

const Counter = ({ add, count }) => {
    return (
        <p>{`${add} plus current count is ${add + count}`}</p>
    )
}

class App extends Component {

    state = {
        count: 0,
    }

    handleClick = () => {
        this.setState({
            count: this.state.count + 1
        }, () => {
            console.log(100 + this.state.count)
        })
    }

    render() {
        return (
            <div>
                <button onClick={this.handleClick}>count up</button>
                <Counter add={100} count={this.state.count}/>
            </div>
        )
    }

}

class App extends Component {

    state = {
        count: 0
    }

    handleClick = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    componentDidUpdate = () => {
        console.log(100 + this.state.count)
    }

    render() {
        return (
            <div>
                <button onClick={this.handleClick}>count up</button>
                <Counter add={100} count={this.state.count}/>
            </div>
        )
    }

}

// With 🥰 to functional programing
// Functional style λ
import { useState, useEffect } from 'react'

const Counter = ({ add, count }) => {
    return (
        <p>{`${add} plus current count is ${add + count}`}</p>
    )
}

const App = () => {

    const add = 100
    const [ count, setCount ] = useState(0)

    useEffect(() => {
        console.log(add + count)
    }, [ count ])

    return (
        <div>
            <button onClick={() => setCount(count + 1)}>count up</button>
            <Counter add={add} count={count} />
        </div>
    )
}
